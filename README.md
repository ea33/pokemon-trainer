# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.3.
## Description
This project is an assignment for Noroff Fullstack Accelerate course. The purpose of the project is to learn Angular.js. The application constists of an Angular frontend and an external api to serve data.

## Install
Clone this repository. Install node and install angular with npm. Add apiTrainers and apiKey to environment.ts. These are provided in moodle. To run the application type ng serve using a cli.

## Usage
The application is used for catching and displaying Pokemon charaters. Once logged in the user is navigated to the catalog page, where they can view a list of pokemon. Using the links on the top of the page the user can access encounter and profile. In the encounter page the user is presented with an image of a pokemon what they can catch if they type the correct name. In the profile page the user sees a list of pokemon in their team. They can remove existing members in the profile page and add new members in the catalog page.

## Contributors
Valtteri Elo and Mikko Kaipainen as developers.
Api made by: https://github.com/dewald-els


