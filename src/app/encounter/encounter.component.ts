/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/
/*WIP pls dont grade encounter*/

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PokeObject } from '../models/pokeObject.model';
import { ShareDataService } from '../services/share-data.service';
import { UpdateService } from '../services/update.service';

@Component({
  selector: 'app-encounter',
  templateUrl: './encounter.component.html',
  styleUrls: ['./encounter.component.css']
})
export class EncounterComponent implements OnInit {
  username:string = ""
  fullPokemonList:PokeObject[] = []
  randomPokemon:PokeObject = {} as PokeObject
  id: Number = 0;
  pokemonList: string[] = []
  constructor(private shareDataService: ShareDataService, private updateService: UpdateService) { }
  randomEncounter(): PokeObject {
    const randomPokemon = this.fullPokemonList[Math.floor(Math.random()*this.fullPokemonList.length)]
    return randomPokemon
  }
  checkAnswer(input: string): void {
    console.log(input, this.randomPokemon['name'], this.id)
    if (input == this.randomPokemon['name']) {
      console.log('menee patchiin')
      this.pokemonList.push(this.randomPokemon['name'])
      this.updateService.updatePokemon(this.pokemonList,this.username,this.id).subscribe()
    }
    else{this.randomPokemon = this.randomEncounter()}
  }

  ngOnInit(): void {
    this.shareDataService.checkLocalStorage()
    this.shareDataService.observeUsername.subscribe(res => this.username = res)
    this.shareDataService.observeParsedPokemon.subscribe(res => this.fullPokemonList = res)
    this.shareDataService.observePokemonList.subscribe(sharedPokemonList => this.pokemonList = sharedPokemonList)
    this.shareDataService.observeId.subscribe(sharedId => this.id = sharedId)
    //this.fullPokemonList.push({entry:'001',img:`https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png`, name: 'bulbasaur', url: 'test'})
    this.randomPokemon = this.randomEncounter()
    console.log(this.fullPokemonList)

  }

}
