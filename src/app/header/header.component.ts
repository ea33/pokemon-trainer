import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShareDataService } from '../services/share-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private shareDataService: ShareDataService) { }
  username: string = ""

  logout(): void{
    localStorage.setItem("username", "undefined")
    localStorage.setItem("loggedIn", "false")
    localStorage.setItem("id", "0")
    this.router.navigate([""])
  }

  ngOnInit(): void {
    this.shareDataService.observeUsername.subscribe(res => this.username = res)
  }

}
