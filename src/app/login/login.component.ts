import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { PokeObject } from '../models/pokeObject.model';
import { FetchedPokemon, RawData } from '../models/raw.model';
import { Trainer } from '../models/trainer.model';
import { LoginService } from '../services/login.service';
import { PokemondataService } from '../services/pokemondata.service';
import { ShareDataService } from '../services/share-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private shareDataService: ShareDataService, private readonly loginService: LoginService,
     private readonly pokemonData : PokemondataService) { }

  username:string = ""
  pokemonList:string[] = []
  loggedIn:boolean = false

  rawData : RawData = {results:[]}
  fetchedPokemon : FetchedPokemon[] = []
  parsedPokemonData : PokeObject[] = []


  Login(input:string): void{
    this.shareDataService.getApiContent()
    this.shareDataService.setUsername(input)
    this.shareDataService.setLoggedIn(true)
    this.username = this.shareDataService.sharedUsername.value
    localStorage.setItem("username", input)
    localStorage.setItem("loggedIn", "true")
    this.pokemonData.getPartyPokemon(input).subscribe((res) => {
      if(res!==undefined){
        localStorage.setItem("partyPokemon" , JSON.stringify(res))
      }
      this.loginService.login(input)
    })
  }

  ngOnInit(): void {
    localStorage.setItem("username", "undefined")
    localStorage.setItem("loggedIn", "false")
    localStorage.setItem("id", "0")
    localStorage.setItem("partyPokemon", "[]")
    this.shareDataService.observeUsername.subscribe(sharedUsername => this.username = sharedUsername)
    this.shareDataService.observePokemonList.subscribe(sharedPokemonList => this.pokemonList = sharedPokemonList)
    
    


    this.pokemonData.getPokemonData().subscribe((res:RawData) =>{
      this.rawData = res
      console.log('getPokemonData')
      //extract list of pokemon from RawData result object
      this.fetchedPokemon = this.rawData.results
      this.fetchedPokemon.forEach((el, i) => {
        let entry:string=this.pokemonData.parseEntry(i)
        //create a new set of parsed data objects that will be rendered on view
        this.parsedPokemonData.push({
            entry: entry,
            img: `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${entry}.png`,
            name: el.name,
            url: el.url
          })
      })
      this.shareDataService.setParsedPokemon(this.parsedPokemonData)
      //console.log(this.shareDataService.sharedParsedPokemon.value)
    })
  }





}
