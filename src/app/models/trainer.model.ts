export interface Trainer {
    id: number;
    username: string;
    pokemon: string[];
    partyPokemon: string[];
    catalogPokemon: string[];
}