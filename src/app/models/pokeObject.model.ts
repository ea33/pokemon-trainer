export interface PokeObject {
    entry:string,
    img:string,
    name:string,
    url:string
}