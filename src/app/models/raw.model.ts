export interface RawData {
    count?: number;
    next?: string;
    previous?: any;
    results: FetchedPokemon[];
}

export interface FetchedPokemon{
    name:string,
    url:string
}