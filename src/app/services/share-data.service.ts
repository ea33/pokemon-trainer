import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PokeObject } from '../models/pokeObject.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShareDataService {

  apiTrainers:string = environment.apiTrainers

  //Username
  sharedUsername = new BehaviorSubject("username undefined")
  observeUsername = this.sharedUsername.asObservable()
  //List of pokemon like in the api
  sharedPokemonList = new BehaviorSubject<string[]>([])
  observePokemonList = this.sharedPokemonList.asObservable()

  partyPokemonList: string[] = []

  //Id of the user
  sharedId = new BehaviorSubject<Number>(0)
  observeId = this.sharedId.asObservable()

  //The whole list of pokemon
  sharedParsedPokemon = new BehaviorSubject<PokeObject[]>([])
  observeParsedPokemon = this.sharedParsedPokemon.asObservable()
  //The list of users caught pokemon
  sharedCaughtPokemon = new BehaviorSubject<string[]>([])
  observeCaughtPokemon = this.sharedCaughtPokemon.asObservable()
  
  sharedPartyPokemon = new BehaviorSubject<string[]>([])
  observePartyPokemon = this.sharedPartyPokemon.asObservable()


  isLoggedIn:boolean = false

  constructor(private http: HttpClient) { }

  getApiContent() {
    return this.http.get(this.apiTrainers)
  }

  setUsername(text: string) {
    this.sharedUsername.next(text)
  }
  
  setPokemonList(pokemonList: string[]){
    this.sharedPokemonList.next(pokemonList)
  }

  loadPokemonLocalStorage(): any {
    return JSON.parse(localStorage.getItem("partyPokemon")!)
  }

  updatePartyPokemonList(input: string){
    this.partyPokemonList.push(input)
    console.log(this.partyPokemonList)
    localStorage.setItem("partyPokemon", JSON.stringify(this.partyPokemonList))
  }

  setId(id: Number) {
    this.sharedId.next(id)
  }
  setParsedPokemon(parsedPokemon: PokeObject[]){
    this.sharedParsedPokemon.next(parsedPokemon)
  }

  setCaughtPokemon(caughtPokemon: string[]){
    this.sharedCaughtPokemon.next(caughtPokemon)
  }

  setPartyPokemon(partyPokemon: string[]){
    this.sharedCaughtPokemon.next(partyPokemon)
  }

  setLoggedIn(bool: boolean){
    this.isLoggedIn = bool
  }

  getLoggedIn(): boolean{
    return this.isLoggedIn
  }

  checkLocalStorage(): void{
    this.setUsername(localStorage.getItem("username")!)
    this.setLoggedIn(localStorage.getItem("loggedIn") === "true")
    this.setId(Number(localStorage.getItem("id")))
    this.partyPokemonList = JSON.parse(localStorage.getItem("partyPokemon")!)
  } 
}
