import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ShareDataService } from './share-data.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private shareDataService: ShareDataService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if(this.shareDataService.getLoggedIn() || localStorage.getItem("loggedIn") == "true"){
      return true
    } else {
      this.router.navigate([""])
      return false 
    }
  }
}
