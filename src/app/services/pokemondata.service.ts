import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FetchedPokemon, RawData } from '../models/raw.model';
import { map } from 'rxjs/operators';
import { Trainer } from '../models/trainer.model';
import { environment } from 'src/environments/environment';

const apiTrainers = environment.apiTrainers

@Injectable({
  providedIn: 'root'
})
export class PokemondataService {

  constructor(private readonly http: HttpClient) { }

  getPokemonData() {
    return this.http.get<RawData>("https://pokeapi.co/api/v2/pokemon?limit=151")
    }

  parseEntry(id:number):string{
    let entry = "00"+(id+1).toString()
    if(entry.length == 4) {entry = entry.substring(1)}
    if(entry.length == 5) {entry = entry.substring(2)}
    return entry
  }

  getPartyPokemon(username: string): Observable<string[] | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`)
    .pipe(
        map((response: Trainer[]) => {
            const user = response.pop()
            if (user != undefined) {
              console.log("party pokemon: " + user['partyPokemon'])
              return user['partyPokemon'];
            } else {
              return undefined
            }
        })
    )
}
}
