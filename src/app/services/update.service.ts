import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PokemondataService } from './pokemondata.service';
import { ShareDataService } from './share-data.service';

const apiTrainers = environment.apiTrainers
const apiKey = environment.apiKey

@Injectable({
  providedIn: 'root'
})
export class UpdateService {
    constructor(private readonly http: HttpClient, private shareDataService: ShareDataService,
        private pokemonDataService: PokemondataService) { }

    //function to patch pokedex numbers to the api
    public updatePokemon(pokemon: string[], username: string, id: Number) {
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": apiKey
        })

        let dexEntries: string[] = []
        pokemon.forEach(element => {
            dexEntries.push(this.findByName(element))
        });
        
        const obj3 = {pokemon: pokemon, catalogPokemon: dexEntries}
        console.log(obj3, id)
        return this.http.patch(`${apiTrainers}/${id}`, obj3, {headers})
    }
    //call this function to update profile page's pokemon in API
    public updatePartyPokemon(id: Number, pokemonToAdd: string) {
        console.log("add pokemon " +pokemonToAdd+ " for user id " + id)

        this.shareDataService.updatePartyPokemonList(pokemonToAdd)

        /*this.pokemonDataService.getPartyPokemon(this.shareDataService.sharedUsername.value)
        .subscribe(res => {
            this.shareDataService.setPartyPokemon(res!)
        })*/
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": apiKey
        })
        let party = this.shareDataService.sharedPartyPokemon.value
        party.push(pokemonToAdd)
        const obj = {partyPokemon: this.shareDataService.loadPokemonLocalStorage()}
        console.log(party)
        this.shareDataService.setPartyPokemon(party)
        return this.http.patch(`${apiTrainers}/${id}`, obj, {headers})
    }

    // finds the pokemon's pokedex entry from the catalogue
    private findByName(name: string) {
        let pokemons = this.shareDataService.sharedParsedPokemon.value
        const foundMon = pokemons.find(element => element['name'] == name)
        if (foundMon) {
            return foundMon['entry']
        }
        else return "0"
    }

    public removePartyPokemon(id: Number, pokemonList: string[]) {
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": apiKey
        })
        let party = pokemonList
        
        const obj = {partyPokemon: party}

        return this.http.patch(`${apiTrainers}/${id}`, obj, {headers})
    }
}