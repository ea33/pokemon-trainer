import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
import { Router } from '@angular/router';
import { ShareDataService } from './share-data.service';

const apiTrainers = environment.apiTrainers
const apiKey = environment.apiKey

@Injectable({
  providedIn: 'root'
})
export class LoginService {
    apiData: any[] = []
    usernameExists: boolean = false

  constructor(private http: HttpClient, private router: Router, private shareData: ShareDataService) { }

  public login(username: string): void {
    this.createTrainer(username)
    this.router.navigateByUrl("/catalogue")
}

    private createTrainer(username: string): void {
        //check if username is already in use
        const content = this.shareData.getApiContent()
        content.subscribe(res => {
            this.apiData.push(res)
            this.apiData[0].forEach((element: { username: string; id: number }) => {
                if(element.username === username){
                    this.shareData.setUsername(element.username)
                    this.shareData.setId(element.id)
                    localStorage.setItem("id", element.id.toString())
                    console.log("loaded existing user")
                    this.usernameExists = true
                    return
                }
            });
            //if username isn't in use, create a new trainer
            if(!this.usernameExists){
                const trainer = {
                    username,
                    catalogPokemon: [],
                    partyPokemon: []
                }
                const headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "x-api-key": apiKey
                })
                console.log('created a new trainer')
                this.http.post<Trainer>(apiTrainers, trainer, {headers}).subscribe(res =>{
                    this.shareData.setUsername(res.username)
                    this.shareData.setId(res.id)
                    localStorage.setItem("id", res.id.toString())
                })
            } else {
                return
            }
        })
    }
}
