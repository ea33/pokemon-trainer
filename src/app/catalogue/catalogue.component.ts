import { Component, OnInit, TemplateRef } from '@angular/core';
import { PokeObject } from '../models/pokeObject.model';
import { FetchedPokemon, RawData } from '../models/raw.model';
import { PokemondataService } from '../services/pokemondata.service';
import { ShareDataService } from '../services/share-data.service';
import { UpdateService } from '../services/update.service';


@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  rawData : RawData = {results:[]}
  fetchedPokemon : FetchedPokemon[] = []
  parsedPokemonData : PokeObject[] = []
  isHidden: boolean = true
  imgSrc: string = ""

  currentPokemonName : string = ""
  currentPokemonUrl: string = ""
  currentPokemonEntry: string = ""
  currentUserId: Number = 0

  constructor(
    private pokemonData: PokemondataService, private sharedData: ShareDataService,
    private updateService : UpdateService
    ) { 
  }

  log(url:string, name:string, img:string, entry:string): void{
    this.currentPokemonName = name
    this.currentPokemonUrl = url
    this.currentPokemonEntry = entry
    console.log("clicked object " + url)
    document.getElementById("modalHeader")!.innerHTML = this.currentPokemonName
    this.imgSrc = img
    this.isHidden = false
  }

  closeModal():void{
    this.isHidden = true
  }

  addToParty():void {
    this.updateService.updatePartyPokemon(this.currentUserId, this.currentPokemonEntry).subscribe()
    this.isHidden = true
    let snackBar = document.getElementById("snackbar");
    snackBar!.innerHTML = this.currentPokemonName + " was added to your party!"
    snackBar!.className = "show";
    setTimeout(function(){ snackBar!.className = snackBar!.className.replace("show", ""); }, 1500);
  }

  ngOnInit(): void {
    this.sharedData.observeId.subscribe(res => this.currentUserId = res)
    this.sharedData.checkLocalStorage()
    this.pokemonData.getPokemonData().subscribe((res:RawData) =>{
      this.rawData = res
      //extract list of pokemon from RawData result object
      this.fetchedPokemon = this.rawData.results
      this.fetchedPokemon.forEach((el, i) => {
        let entry:string=this.pokemonData.parseEntry(i)
        //create a new set of parsed data objects that will be rendered on view
        this.parsedPokemonData.push({
            entry: entry,
            img: `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${entry}.png`,
            name: el.name,
            url: el.url
          })
      })
      this.sharedData.setParsedPokemon(this.parsedPokemonData)
      localStorage.setItem("allPokemon", JSON.stringify(this.parsedPokemonData))
    })
  }

}
