import { Component, OnInit, OnChanges } from '@angular/core';
import { LoginService } from '../services/login.service';
import { UpdateService } from '../services/update.service';
import { ShareDataService } from '../services/share-data.service';
import { PokemondataService } from '../services/pokemondata.service';
import { PokeObject } from '../models/pokeObject.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  partyPokemon: string[] = []
  username: string = ""
  parsedPokemon:PokeObject[] = []

  constructor(private shareDataService: ShareDataService , private readonly loginService: LoginService, private readonly updateService: UpdateService,
    private readonly pokemonDataService: PokemondataService) { }

  pokemonList:PokeObject[] = []
  id:Number = 0

  partyRemove(entry: string) {
    let element = document.getElementById(entry)
    element?.remove()
    this.partyPokemon = this.partyPokemon.filter(pokemon => pokemon !== entry)
    console.log(this.partyPokemon)
    this.shareDataService.setPartyPokemon(this.partyPokemon)
    this.updateService.removePartyPokemon(this.id, this.partyPokemon).subscribe( res => {
      this.pokemonDataService.getPartyPokemon(this.username).subscribe(
        res => localStorage.setItem("partyPokemon", JSON.stringify(res))
      )
    })
  }
  

  ngOnInit(): void {
    this.shareDataService.checkLocalStorage()
    this.partyPokemon = JSON.parse(localStorage.getItem("partyPokemon")!)
    this.shareDataService.observeUsername.subscribe(sharedUsername => this.username = sharedUsername)
    this.shareDataService.observeId.subscribe(sharedId => this.id = sharedId)
    this.shareDataService.observeParsedPokemon.subscribe(parsedPokemon => this.parsedPokemon = parsedPokemon)
    this.pokemonDataService.getPartyPokemon(this.username)
    .subscribe({
      next: party => {
        if (party != undefined) {
          this.partyPokemon = party
          this.partyPokemon.forEach(entry => {
            let pokemon = JSON.parse(localStorage.getItem("allPokemon")!)
            .find((element: { [x: string]: string; }) => element['entry'] == entry)
            if (pokemon != undefined) {
              this.pokemonList.push(pokemon)
            }
          });
        }
      }
    })
  }

  
}