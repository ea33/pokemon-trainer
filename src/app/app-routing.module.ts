import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { EncounterComponent } from './encounter/encounter.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { GuardService } from './services/guard.service';

const routes: Routes = [
  {path: "", component:LoginComponent},
  {path: "catalogue", component:CatalogueComponent, canActivate: [GuardService]},
  {path: "profile", component:ProfileComponent, canActivate: [GuardService]},
  //{path: "encounter", component:EncounterComponent, canActivate: [GuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
